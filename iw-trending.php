<?php
/*
Plugin Name: IW Trending
Plugin URI: https://instruction.austincc.edu/
Description: Insert trending links
Version: 1.01
Author: Shea Scott
Author URI: 
License: GPL2
*/

$iwTrendingPluginURI = plugins_url('/iw-trending/');

// The widget class
class IW_Trending_Widget extends WP_Widget {

	// Main constructor
	public function __construct() {
        parent::__construct(
            'iw_trending_widget',
            __( 'IW Trending', 'text_domain' ),
            array(
                'customize_selective_refresh' => true,
            )
        );
	}

	// The widget form (for the backend )
	public function form( $instance ) {	
   
        global $iwTrendingPluginURI;

        wp_register_style( 'iw_trending_admin_css', $iwTrendingPluginURI . 'css/iw-trending-admin-styles.css', false, '1.0' );
        wp_enqueue_style( 'iw_trending_admin_css' );

        // Set widget defaults
        $defaults = array(
            'url1'    => '',
            'text1'    => '',
            'url2'    => '',
            'text2'    => '',
        );
        
        // Parse current settings with defaults
        extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>
        <div id="iw-trending">
            <?php // Widget Title ?>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', 'text_domain' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
            </p>

            <p>Links will appear in order.</p>

            <?php // create input fields for links ?>
            <?php 
            
                for ($i = 1; $i <= 5; $i++) {
                    $value = esc_attr( ${"file" . $i} ); 
                    echo "<fieldset><legend>Link $i</legend>";
                    echo '<label for="' . esc_attr( $this->get_field_id( 'url' . $i ) ) . '">';
                    _e( 'URL', 'text_domain' );
                    echo '</label>';
                    echo '<input class="widefat" id="' . esc_attr( $this->get_field_id( 'url' . $i ) ) . '" name="' . esc_attr( $this->get_field_name( 'url' . $i ) ) . '" type="text" value="' . ${"url{$i}"} . '"/>';
                    // Link text
                    echo '<label for="' . esc_attr( $this->get_field_id( 'text' . $i ) ) . '">';
                    _e( 'Text', 'text_domain' );
                    echo '</label>';
                    echo '<input class="widefat" id="' . esc_attr( $this->get_field_id( 'text' . $i ) ) . '" name="' . esc_attr( $this->get_field_name( 'text' . $i ) ) . '" type="text" value="' . ${"text{$i}"}  . '"/>';
                    echo '</fieldset>';
                }
            
            ?>
        </div>
<?php }

	// Update widget settings
	public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance["title"]    = isset( $new_instance["title"] ) ? wp_strip_all_tags( $new_instance["title"] ) : '';
        for ($i = 1; $i <= 5; $i++) {
            $instance["url$i"]    = isset( $new_instance["url$i"] ) ? wp_strip_all_tags( $new_instance["url$i"] ) : '';
            $instance["text$i"]    = isset( $new_instance["text$i"] ) ? wp_strip_all_tags( $new_instance["text$i"] ) : '';
        }
        return $instance;
	}


	// Display the widget
	public function widget( $args, $instance ) {

        global $iwTrendingPluginURI;

        extract( $args );

        // Check the widget options
        wp_register_style( 'iw_trending_css', $iwTrendingPluginURI . "css/iw-trending-base.css", false, '1.0' );
        wp_enqueue_style( 'iw_trending_css' );

        function iw_trending_widget_enqueue_script() {   
            wp_register_script( 'trending_script', plugins_url( 'js/iw-trending.js', __FILE__ ), array( 'jquery' ), false );
            wp_enqueue_script( 'trending_script');
        }
        add_action('wp_enqueue_scripts', 'iw_trending_widget_enqueue_script');

        // WordPress core before_widget hook (always include )
        echo $before_widget;

        echo "<div id='iw-trending-container'>";
        // echo "<a href='https://www.austincc.edu/coronavirus' target='_blank'>COVID-19 Updates</a>";
;        for ($i = 1; $i <= 5; $i++) {
            if ( $instance["url$i"] && $instance["text$i"] ) {
                $max_char = 100;
                $url = $instance["url$i"];
                $text = $instance["text$i"];
                $truncated_text = strlen($text) > $max_char ? substr( $text, 0, $max_char ) . "&hellip;" : $text; 
                echo "<a href='$url'>$truncated_text</a>";
            }
        }
    
        echo "</div>";

        // WordPress core after_widget hook (always include )
        echo $after_widget;
	}

}

// Register the widget
function my_register_iw_trending_widget() {
	register_widget( 'IW_Trending_Widget' );
}
add_action( 'widgets_init', 'my_register_iw_trending_widget' );