// JS
jq2 = jQuery.noConflict();
jq2(function ($) {
  $(document).ready(function () {
    var container = $("#iw-trending-container");
    $("#covid-text").hide();
    $("#covid-text").after(container);

    var fadeSpeed = 500;
    var linkTotal = $("#iw-trending-container > a").length;
    $("#iw-trending-container > a:first-child").fadeIn(fadeSpeed);
    var visibleLink = 2;
    setInterval(function () {
      $("#iw-trending-container > a").hide();
      $("#iw-trending-container > a:nth-child(" + visibleLink + ")").fadeIn(
        fadeSpeed
      );
      if (visibleLink >= linkTotal) {
        visibleLink = 1;
      } else {
        visibleLink++;
      }
    }, 5000);
  });
});
